all:
	echo "hsc3-meta"

clean:
	(cd cmd ; make clean)
	rm -Rf dist dist-newstyle

mk-cmd:
	(cd cmd ; make all install)

mk-tags:
	find . -name '*.hs' | xargs hasktags -e

push-all:
	r.gitlab-push.sh hsc3-meta
