hsc3-meta
---------

[hsc3-meta](http://rohandrape.net/?t=hsc3-meta)
is meta for
[hsc3](http://rohandrape.net/?t=hsc3)

## cli

[eval](http://rohandrape.net/?t=hsc3-meta&e=md/eval.md),
[setup](http://rohandrape.net/?t=hsc3-meta&e=md/setup.md)

## notes

To remove working submodule files: `(for i in lib/* ; do git submodule deinit -f $i ; done)`

To update all submodule references to track current remote: `git submodule foreach "(git checkout master; git pull)&"`

(At present this fails for recent projects because _gitlab_ has changed the default branch name from master to main, c.f. stsc3-lisp and stsc3-som)

© [rohan drape](http://rohandrape.net/), 2006-2023, [gpl](http://gnu.org/copyleft/)
