{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad {- base -}
import System.Exit {- base -}

import Control.Monad.Loops {- monad-loops -}

import qualified Language.Haskell.Interpreter as Hint {- hint -}

import Sound.Sc3.Common.Context {- hsc3 -}

context_to_module_imports :: Context -> [Hint.ModuleImport]
context_to_module_imports =
  let f (moduleName,qualifier,_packageName) =
        case qualifier of
          Nothing -> Hint.ModuleImport moduleName Hint.NotQualified Hint.NoImportList
          Just qualifierName -> Hint.ModuleImport moduleName (Hint.ImportAs qualifierName) Hint.NoImportList
  in map f

getInput :: IO [String]
getInput = do
  l1 <- getLine
  if l1 == ":{" then unfoldWhileM (/= ":}") getLine else return [l1]

getInputText :: IO String
getInputText = fmap unlines getInput

evalStep :: Hint.InterpreterT IO ()
evalStep = do
  txt <- Hint.liftIO getInputText
  let verboseMode = False
  when verboseMode (Hint.liftIO (putStrLn ("'" ++ txt ++ "'")))
  when (txt == ":quit\n") (Hint.liftIO (exitWith ExitSuccess))
  chk <- Hint.typeChecksWithDetails txt
  case chk of
    Right _typ -> Hint.runStmt txt
    Left err -> Hint.liftIO (putStrLn (unlines (map Hint.errMsg err)) >> putStrLn txt)

eval :: IO ()
eval = do
  let useExt = False
      setExt = Hint.set [Hint.languageExtensions Hint.:= [Hint.OverloadedLists]]
      setImp = Hint.setImportsF (context_to_module_imports std_context)
      repl = forever evalStep
  r <- Hint.runInterpreter (when useExt setExt >> setImp >> repl)
  case r of
    Left err -> error (show err) >> eval
    Right () -> return ()

main :: IO ()
main = eval
