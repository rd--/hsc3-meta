# hsc3-setup (May, 2015)

Co-ordination of [hsc3](http://rohandrape.net/t/hsc3) related packages.

There is a _make_ rule _build-setup_ that builds and installs the
_hsc3-setup_ executable, which depends only on standard haskell
packages (base, directory, filepath, process, split).

The package db is stored at `hsc3-meta/db/setup.db`.

## echo

Print all projects in named category.

Categories are printed by `hsc3-setup -h`.

~~~~
hsc3-setup echo all
~~~~

## env

Run `cabal env` to install local packages.

~~~~
hsc3-setup env install core ~/sw
~~~~

## clone & update

Clone or update packages in _category_ from _source_ repositories to _destination_.

~~~~
hsc3-setup clone core https://gitlab.com/rd-- /tmp
hsc3-setup update core https://gitlab.com/rd-- ~/sw
~~~~

## local

Run a command at each local directory for _category_.

Common operations are to clean, check for local edits and push any changes to remote.

The string `@pkg` is replaced with the current package name.

~~~~
hsc3-setup local all ~/sw cabal clean
hsc3-setup local all ~/sw darcs wh -ls
hsc3-setup local all ~/sw make push-rd
hsc3-setup local all ~/sw make mk-cmd
hsc3-setup local all ~/sw cabal-env --local -t -i @pkg
~~~~

# pkg-dep

Print package dependencies written as comments following import statements.

~~~~
$ for i in $(hsc3-setup pkg-dep -non-local ~/sw/hsc3-graphs/gr/*.hs) ; do echo $i ; done
MonadRandom
array
base
containers
directory
filepath
hashable
primes
process
random
random-shuffle
split
$
~~~~

# bootstrap

To setup all _hsc3_ packages on a new machine, given _git_ & _ghc_ &
_cabal_, and setting LOCAL and PREFIX appropriately:

~~~~
LOCAL=...
PREFIX=...
REMOTE=https://gitlab.com/rd--
mkdir -p $LOCAL
cd $LOCAL
git clone $REMOTE/hsc3-meta/
(cd hsc3-meta/cmd ; prefix=$PREFIX make build-setup)
hsc3-setup clone all $REMOTE $LOCAL
hsc3-setup env install all $LOCAL
~~~~
