git submodule add https://gitlab.com/rd--/hosc
git submodule add https://gitlab.com/rd--/hsc3
git submodule add https://gitlab.com/rd--/hmt-base
git submodule add https://gitlab.com/rd--/html-minus
git submodule add https://gitlab.com/rd--/hosc-util
git submodule add https://gitlab.com/rd--/hsc3-db
git submodule add https://gitlab.com/rd--/hsc3-dot
git submodule add https://gitlab.com/rd--/hsc3-lang
git submodule add https://gitlab.com/rd--/hsc3-plot
git submodule add https://gitlab.com/rd--/hsc3-sf
git submodule add https://gitlab.com/rd--/hsc3-sf-hsndfile
git submodule add https://gitlab.com/rd--/hsc3-ui
git submodule add https://gitlab.com/rd--/sc3-rdu
git submodule add https://gitlab.com/rd--/hcg-minus
git submodule add https://gitlab.com/rd--/hcg-minus-cairo
git submodule add https://gitlab.com/rd--/hps
git submodule add https://gitlab.com/rd--/www-minus
git submodule add https://gitlab.com/rd--/after-pim
git submodule add https://gitlab.com/rd--/hmeap
git submodule add https://gitlab.com/rd--/hmt
git submodule add https://gitlab.com/rd--/hosc-json
git submodule add https://gitlab.com/rd--/hsc3-auditor
git submodule add https://gitlab.com/rd--/hsc3-cairo
git submodule add https://gitlab.com/rd--/hsc3-data
git submodule add https://gitlab.com/rd--/hsc3-m
git submodule add https://gitlab.com/rd--/hsc3-rec
git submodule add https://gitlab.com/rd--/hsc3-rw
git submodule add https://gitlab.com/rd--/hsdif
git submodule add https://gitlab.com/rd--/hsharc
git submodule add https://gitlab.com/rd--/hspear
git submodule add https://gitlab.com/rd--/midi-osc
git submodule add https://gitlab.com/rd--/rju
git submodule add https://gitlab.com/rd--/tctl
git submodule add https://gitlab.com/rd--/hsc3-forth
git submodule add https://gitlab.com/rd--/hsc3-lisp
git submodule add https://gitlab.com/rd--/stsc3
git submodule add https://gitlab.com/rd--/stsc3-som
git submodule add https://gitlab.com/rd--/stsc3-lisp
git submodule add https://gitlab.com/rd--/hdf
git submodule add https://gitlab.com/rd--/hls
git submodule add https://gitlab.com/rd--/hly
git submodule add https://gitlab.com/rd--/hmt-diagrams
git submodule add https://gitlab.com/rd--/hps-cairo
git submodule add https://gitlab.com/rd--/hts
git submodule add https://gitlab.com/rd--/img-osc

# git submodule add https://gitlab.com/rd--/hsc3-process
# git submodule add https://gitlab.com/rd--/hsc3-server

# git submodule add https://gitlab.com/rd--/hsc3-unsafe
